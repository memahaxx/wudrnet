'''
This script automates finding locations in athr.sys drivers to patch for drc compatibility.

Steps to patch and run with atheros driver:
 1) Install original driver
 2) Enable test signing
 3) Run this script to find places to patch
 4) Manually find TSF patch
 5) Apply patches and fix PE checksum (CheckSumMappedFile)
 6) Create test cert and sign the modified binary
 7) Replace original driver and reload device
 
In the future this should be done as runtime hooking, so it's easier to have the
driver still work with non-Nintendo-OUI frames...and gets around the signing issue.
'''

from idaapi import *
from idautils import *
from idc import *

cipher_array = '00 0F AC 01 00 0F AC 02 00 0F AC 03 00 0F AC 04 00 0F AC 04 00 40 96 00 00 0F AC 06 00 0F AC 00'
key_mgmt = '00 0F AC 02'

if __name__ == '__main__':
    rdataSeg = get_segm_by_name('.rdata')
    cipherArrayEA = FindBinary(rdataSeg.startEA, SEARCH_DOWN | SEARCH_CASE, cipher_array)
    if cipherArrayEA == BADADDR:
        print 'couldn\'t find cipher array'
    print 'cipher array %x, %x' % (cipherArrayEA + 4 * 3, cipherArrayEA + 4 * 4)
    
    dref = 0
    dref_func = None
    while True:
        dref = get_next_dref_to(cipherArrayEA, dref)
        if dref == BADADDR: break
        if dref_func is None:
            dref_func = get_func(dref)
            if dref_func is None: continue
        elif dref_func != get_func(dref):
            print 'multiple dref functions...'
    #print 'dref func %x' % (dref_func.startEA)
    
    curEA, endEA = rdataSeg.startEA, rdataSeg.endEA
    while curEA < endEA:
        foundEA = FindBinary(curEA, SEARCH_DOWN | SEARCH_CASE, key_mgmt)
        if foundEA == BADADDR: break
        dref = get_next_dref_to(foundEA, 0)
        if dref != BADADDR:
            dref_data = get_func(dref)
            if dref_data is not None:
                if dref_data == dref_func:
                    print 'key_mgmt %x' % (foundEA)
                    break
        curEA = foundEA + len(key_mgmt)
        
    # TODO find location of indirect call, and the proper replacement value (for tsf)
    