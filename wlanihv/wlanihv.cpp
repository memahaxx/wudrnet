#include "stdafx.h"
#include <cstdint>
#include <Windows.h>
#include <Wlanapi.h>
#pragma comment(lib, "Wlanapi.lib")
#include <complex>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

PWLAN_INTERFACE_INFO GetAtherosInterface(PWLAN_INTERFACE_INFO_LIST InterfaceList)
{
	for (DWORD i = 0; i < InterfaceList->dwNumberOfItems; ++i)
	{
		PWLAN_INTERFACE_INFO InterfaceInfo = &InterfaceList->InterfaceInfo[i];
		_tprintf(TEXT("%s %i\n"), InterfaceInfo->strInterfaceDescription, InterfaceInfo->isState);
		if (_tcsstr(InterfaceInfo->strInterfaceDescription, TEXT("Atheros")) != nullptr) // broken on zero-length string
		{
			return InterfaceInfo;
		}
	}

	return nullptr;
}

void TestShit()
{
	// Open

	DWORD Version, ChosenVersion;
	HANDLE ClientHandle;

	Version = WLAN_API_MAKE_VERSION(2, 0);

	DWORD Status = ::WlanOpenHandle(
		Version,
		nullptr,
		&ChosenVersion,
		&ClientHandle
		);
	if (Status != ERROR_SUCCESS)
	{
		printf("WlanOpenHandle:%x\n", Status);
	}
	if (ChosenVersion != Version)
	{
		printf("WlanOpenHandle:wrong version:%x\n", ChosenVersion);
	}

	PWLAN_INTERFACE_INFO_LIST InfoList = nullptr;
	Status = WlanEnumInterfaces(ClientHandle, nullptr, &InfoList);
	if (Status != ERROR_SUCCESS)
	{
		printf("WlanEnumInterfaces:%x\n", Status);
	}

	WLAN_INTERFACE_INFO *AtherosInterface = GetAtherosInterface(InfoList);

	// need to grant WLAN_WRITE_ACCESS for non-admin
	//WlanSetSecuritySettings(ClientHandle, WLAN_SECURABLE_OBJECT::wlan_secure_ihv_control, );

	// Send IhvControl
	struct IhvCtrlHeader
	{
		DWORD ReqType;
		DWORD Oid;
		DWORD Unk[2];
		DWORD RespSize;
		DWORD Unk2;
	};
	static_assert(sizeof(IhvCtrlHeader) == 0x18, "IhvCtrlHeader wrong size");

	struct TsfResponse
	{
		u8 tsf[4];
	};
	static_assert(sizeof(TsfResponse) == 4, "TsfResponse wrong size");

	IhvCtrlHeader InBuffer = { 0 };
	u8 OutBuffer[sizeof(IhvCtrlHeader)+sizeof(TsfResponse)] = { 0 };
	DWORD InBufferSize = sizeof(InBuffer),
		OutBufferSize = sizeof(OutBuffer),
		NumBytesReturned;

	// Query the oid handler we've patched
	InBuffer.ReqType = 0;
	InBuffer.Oid = 0xFF000043;

	u32 sysStart = 0;
	u32 tsfStart = 0;

	for (int i = 0; i < 1000; i++)
	{
		Status = ::WlanIhvControl(
			ClientHandle,
			&AtherosInterface->InterfaceGuid,
			WLAN_IHV_CONTROL_TYPE::wlan_ihv_control_type_driver,
			InBufferSize,
			(PVOID)&InBuffer,
			OutBufferSize,
			(PVOID)OutBuffer,
			&NumBytesReturned
			);
		if (Status != ERROR_SUCCESS)
		{
			printf("WlanIhvControl:%x\n", Status);
		}

		if (NumBytesReturned != sizeof(IhvCtrlHeader)+sizeof(TsfResponse))
		{
			printf("wtf\n");
		}

		TsfResponse *pTsf = (TsfResponse *)&OutBuffer[sizeof(IhvCtrlHeader)];

		//printf("WlanIhvControl response data: %8x\n", *(u32 *)pTsf->tsf);
		u32 tsfTime = *(u32 *)pTsf->tsf;
		u32 sysTime = ::GetTickCount();
		if (tsfStart == 0) tsfStart = tsfTime;
		if (sysStart == 0) sysStart = sysTime;

		s32 sysDelta = (sysTime - sysStart) * 1000;
		s32 tsfDelta = tsfTime - tsfStart;

		u32 delta = std::abs(sysDelta - tsfDelta) / 1000;
		printf("%8x %8x %i\n", sysDelta, tsfDelta, delta);
	}

	// Close

	if (InfoList != nullptr)
	{
		WlanFreeMemory(InfoList);
		InfoList = nullptr;
	}

	Status = ::WlanCloseHandle(ClientHandle, nullptr);
	if (Status != ERROR_SUCCESS)
	{
		printf("WlanCloseHandle:%x\n", Status);
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	TestShit();

	return 0;
}
