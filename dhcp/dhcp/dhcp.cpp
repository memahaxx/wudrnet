#define _CRT_SECURE_NO_WARNINGS

#include <stdint.h>
#include <stdio.h>
#include <string.h>
/*
#include <unistd.h>
#include <errno.h>
#include <pwd.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
*/
#include <WinSock2.h>
#pragma comment(lib, "ws2_32.lib")
typedef uint32_t in_addr_t;

enum
{
  PORT_BOOTPS = 67,
  PORT_BOOTPC
};

enum
{
    BOOTP_OP_REQ = 1,
    BOOTP_OP_REPLY
};

enum
{
    DHCP_TYPE_DISCOVER = 1,
    DHCP_TYPE_OFFER,
    DHCP_TYPE_REQUEST,
    DHCP_TYPE_DECLINE,
    DHCP_TYPE_ACK,
    DHCP_TYPE_NACK,
    DHCP_TYPE_RELEASE,
    DHCP_TYPE_INFORM
};

enum { DHCP_OPTIONS_OFFSET = 0xec };
static uint8_t const dhcp_magic[] = { 0x63, 0x82, 0x53, 0x63 };

enum
{
    DHCP_OPTION_PAD         = 0x00,
    DHCP_OPTION_SUBNET_MASK = 0x01,
    DHCP_OPTION_ROUTER      = 0x03,
    DHCP_OPTION_REQ_IP_ADDR = 0x32,
    DHCP_OPTION_LEASE_TIME  = 0x33,
    DHCP_OPTION_MSG_TYPE    = 0x35,
    DHCP_OPTION_SERVER_ID   = 0x36,
    DHCP_OPTION_PARAM_LIST  = 0x37,
    DHCP_OPTION_END         = 0xff
};

enum { MTU_SIZE = 1500 };

struct ip_config_t
{
    in_addr_t broadcast;
    in_addr_t intf;
    in_addr_t lease;
    uint8_t mac[6];
} const ip_config =
{
    inet_addr("255.255.255.255"),
    inet_addr("192.168.1.10"),
    inet_addr("192.168.1.11"),
    { 0x40, 0xf4, 0x07, 0x8f, 0x65, 0xcc }
};

int main()
{
	WSADATA WsaData;
	if (WSAStartup(MAKEWORD(2, 2), &WsaData) != 0)
		printf("FAIL\n");

    int sock_recv = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
    int sock_send = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
	struct sockaddr_in addr = { AF_INET, htons(PORT_BOOTPS) };

	addr.sin_addr.s_addr = inet_addr("0.0.0.0");
    if (bind(sock_recv, (struct sockaddr *)&addr, sizeof(addr)) != 0)
    {
		fprintf(stderr, "%i %i\n", __LINE__, WSAGetLastError());
		return errno;
    }

    addr.sin_addr.s_addr = { ip_config.intf };
    if (bind(sock_send, (struct sockaddr *)&addr, sizeof(addr)) != 0)
    {
		fprintf(stderr, "%i %i\n", __LINE__, WSAGetLastError());
		return errno;
    }

    int const opt_broadcast_enable = 1;
    setsockopt(sock_send, SOL_SOCKET, SO_BROADCAST,
        (const char *)&opt_broadcast_enable, sizeof(opt_broadcast_enable));
    /*
    char const *opt_iface = "wlan0";
    setsockopt(sock_send, SOL_SOCKET, SO_BINDTODEVICE,
        opt_iface, strlen(opt_iface));
    //*/
	/*
    struct passwd *nobody = getpwnam("nobody");
    if (nobody && nobody->pw_uid)
        setuid(nobody->pw_uid);
	*/
    addr.sin_port = htons(PORT_BOOTPC);
    addr.sin_addr.s_addr = { ip_config.broadcast };

    for (;;)
    {
        fd_set rset;
        FD_ZERO(&rset);
        FD_SET(sock_recv, &rset);
        select(sock_recv + 1, &rset, nullptr, nullptr, nullptr);

        if (FD_ISSET(sock_recv, &rset))
        {
            uint8_t type = 0;
            uint8_t p[MTU_SIZE];
            memset(p, 0xff, MTU_SIZE);

            int num_recv = recv(sock_recv, (char *)p, MTU_SIZE, 0);

            if (num_recv < DHCP_OPTIONS_OFFSET + sizeof(dhcp_magic))
                continue;

            if (memcmp(&p[DHCP_OPTIONS_OFFSET], dhcp_magic, sizeof(dhcp_magic)))
                continue;

            if (p[0] == BOOTP_OP_REPLY)
                continue;

            for (int i = DHCP_OPTIONS_OFFSET + sizeof(dhcp_magic); i < MTU_SIZE;)
            {
                if (p[i] == DHCP_OPTION_END)
                    break;
                
                if (p[i] == DHCP_OPTION_PAD)
                {
                    i++;
                    continue;
                }

                if (p[i] == DHCP_OPTION_MSG_TYPE)
                    type = p[i + 2];

                i += p[i + 1] + 2;
            }

            if (type == DHCP_TYPE_REQUEST)
                printf("OMFG\n");

            if (type == DHCP_TYPE_DISCOVER || type == DHCP_TYPE_REQUEST)
            {
                p[0] = BOOTP_OP_REPLY;

                *(uint32_t *)(p + 16) = (uint32_t)ip_config.lease;

                int i = DHCP_OPTIONS_OFFSET + sizeof(dhcp_magic);

                p[i++] = DHCP_OPTION_MSG_TYPE; p[i++] = 1;
                p[i++] = (type == DHCP_TYPE_DISCOVER) ? DHCP_TYPE_OFFER : DHCP_TYPE_ACK;

                p[i++] = DHCP_OPTION_SUBNET_MASK; p[i++] = 4;
                p[i++] = 255; p[i++] = 255; p[i++] = 255; p[i++] = 0;

                p[i++] = DHCP_OPTION_ROUTER; p[i++] = 4;
                p[i++] = 192; p[i++] = 168; p[i++] = 1; p[i++] = 1;

                p[i++] = DHCP_OPTION_LEASE_TIME; p[i++] = 4;
                *(uint32_t *)(p + i) = 0xffffffff; i += 4;

                p[i++] = DHCP_OPTION_SERVER_ID; p[i++] = 4;
                *(uint32_t *)(p + i) = (uint32_t)ip_config.intf; i += 4;
                
                p[i++] = DHCP_OPTION_END;

                /*
                static useconds_t amount = 10;
                usleep(amount += 50);
                //*/
                
                sendto(sock_send, (char *)p, i, 0,
                    (struct sockaddr *)&addr, sizeof(addr));
            }
        }
    }

	WSACleanup();

    return 0;
}
