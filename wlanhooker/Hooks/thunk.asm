; This file implements a trampoline to be called from a hook
; Hook needs to call HookThunk, which spills regs and calls
; ThunkConverion, which vectors to the appropriate handler.
; On completion, the handler calls HookReturn with the reg
; state to restore, and an optional ptr to code. If present
; the ptr is jumped to after restoring context. Otherwise
; we just execute ret to the hooked func's caller.

PUBLIC	HookThunk
PUBLIC	HookReturn
PUBLIC	Thing
PUBLIC	ThingReturn

EXTRN	?ThunkConversion@@YAXPEAUGPR_CONTEXT@@@Z:PROC
EXTRN	?ThingConversion@@YAXPEAUGPR_CONTEXT@@@Z:PROC

SHADOW_STACK_SIZE EQU 020h

_TEXT	SEGMENT

HookThunk PROC

    ; spill all gpr
	; rip is pushed by caller
	push r15
    push r14
    push r13
    push r12
    push r11
    push r10
    push r9
    push r8
;    push rsp
    push rbp
    push rsi
    push rdi
    push rdx
    push rcx
    push rbx
    push rax

    mov rcx, rsp

	sub rsp, SHADOW_STACK_SIZE

	; just to be safe, don't hardcode the alignment
	and rsp, -010h

    call ?ThunkConversion@@YAXPEAUGPR_CONTEXT@@@Z

	; never reached
	int 3

HookThunk ENDP

; NOTE: Currently rip is not restored from struct
; and rflags isn't saved in the first place
HookReturn PROC
	
	mov rsp, rcx
	; save the (possible) continuation ptr
	mov [rsp - 8], rdx
    
    ; restore gpr
	pop rax
    pop rbx
    pop rcx
    pop rdx
    pop rdi
    pop rsi
    pop rbp
;    pop rsp
    pop r8
    pop r9
    pop r10
    pop r11
    pop r12
    pop r13
    pop r14
    pop r15

	; adjust stack for the hook's 'call'
	add rsp, 8

	; if continuation ptr exists,
	cmp qword ptr [rsp - 8 - 080h], 0
	jz ReturnToCaller

	; jump to it and execute saved bytes,
	; then jmp back to hooked func
	jmp qword ptr [rsp - 8 - 080h]

ReturnToCaller:

	; else return to hooked func's caller
    ret

HookReturn ENDP

Thing PROC

	; extra 8 since we are just arrive here via ret
	sub rsp, 8 * 16 + 8
	mov [rsp + 8 *  0], rax
	mov [rsp + 8 *  1], rbx
	mov [rsp + 8 *  2], rcx
	mov [rsp + 8 *  3], rdx
	mov [rsp + 8 *  4], rdi
	mov [rsp + 8 *  5], rsi
	mov [rsp + 8 *  6], rbp
	mov [rsp + 8 *  7], r8
	mov [rsp + 8 *  8], r9
	mov [rsp + 8 *  9], r10
	mov [rsp + 8 * 10], r11
	mov [rsp + 8 * 11], r12
	mov [rsp + 8 * 12], r13
	mov [rsp + 8 * 13], r14
	mov [rsp + 8 * 14], r15
	;mov [rsp + 8 * 15], 0 ; rip

	mov rcx, rsp

	sub rsp, SHADOW_STACK_SIZE
	and rsp, -010h

	call ?ThingConversion@@YAXPEAUGPR_CONTEXT@@@Z

	int 3

Thing ENDP

ThingReturn PROC

	mov rsp, rcx
	mov rax, [rsp + 8 *  0]
	mov rbx, [rsp + 8 *  1]
	mov rcx, [rsp + 8 *  2]
	mov rdx, [rsp + 8 *  3]
	mov rdi, [rsp + 8 *  4]
	mov rsi, [rsp + 8 *  5]
	mov rbp, [rsp + 8 *  6]
	mov r8 , [rsp + 8 *  7]
	mov r9 , [rsp + 8 *  8]
	mov r10, [rsp + 8 *  9]
	mov r11, [rsp + 8 * 10]
	mov r12, [rsp + 8 * 11]
	mov r13, [rsp + 8 * 12]
	mov r14, [rsp + 8 * 13]
	mov r15, [rsp + 8 * 14]
	; rip

	add rsp, 080h

	ret

ThingReturn ENDP

_TEXT	ENDS

END
