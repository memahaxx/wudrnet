#include "common.h"
#include "ModulePatcher.h"
#include "hde6404c\include\hde64.h"
// XXX for Log
#include "Hooks.h"

// Import the asm stubs
extern "C"
{
extern void HookThunk();
extern void HookReturn(GPR_CONTEXT *Context, uintptr_t Continuation);
extern void Thing();
extern void ThingReturn(GPR_CONTEXT *Context);
}

u8 const HookBytes::OpCall[] = { 0xff, 0x15 };
u8 const HookBytes::OpJmp[] = { 0xff, 0x25 };

HookMap_t ModulePatcher::HookHandlers;
typedef std::pair<uintptr_t, HookEntry_t> ReturnStuff_t;
std::map<uintptr_t, ReturnStuff_t> ThingHandlers;

// Called from asm stub
void
__stdcall
ThunkConversion(
	GPR_CONTEXT *Context
	)
{
	/*
	PTSTR GprNames[] = {
		TEXT("rax"),
		TEXT("rbx"),
		TEXT("rcx"),
		TEXT("rdx"),
		TEXT("rdi"),
		TEXT("rsi"),
		TEXT("rbp"),
		//TEXT("rsp"),
		TEXT("r8"),
		TEXT("r9"),
		TEXT("r10"),
		TEXT("r11"),
		TEXT("r12"),
		TEXT("r13"),
		TEXT("r14"),
		TEXT("r15"),
		TEXT("rip")
	};

	uintptr_t *rsp = (uintptr_t *)(Context + 1);
	Log("rsp:%p -> %p\n", rsp, *rsp);

	for (int i = 0; i < Context->NUM_GPR; ++i)
	{
		Log("%-3s %p\n", GprNames[i], Context->r[i]);
	}
	//*/
	auto const &Funcs = std::get<0>(ModulePatcher::HandlerForHook(Context));
	if (Funcs.second != nullptr)
	{
		// has return hook
		uintptr_t *rsp = (uintptr_t *)(Context + 1);
		ThingHandlers[(uintptr_t)rsp] = std::make_pair(rsp[0], Funcs.second);
		rsp[0] = (uintptr_t)Thing;
	}
	Funcs.first(Context);
}

void
__stdcall
ThingConversion(
	GPR_CONTEXT *Context
	)
{
	uintptr_t *rsp = (uintptr_t *)(Context + 1);
	ThingHandlers[(uintptr_t)rsp].second(Context);
}

// Return to asm stub
void ReturnConversion(GPR_CONTEXT *Context)
{
	HookReturn(Context, std::get<1>(ModulePatcher::HandlerForHook(Context)));
}

void ThingReturnConversion(GPR_CONTEXT *Context)
{
	uintptr_t *rsp = (uintptr_t *)(Context + 1);
	rsp[0] = ThingHandlers[(uintptr_t)rsp].first;
	ThingReturn(Context);
}

ModulePatcher::ModulePatcher()
	: ModuleBase(0)
{
}

ModulePatcher::ModulePatcher(LPCTSTR ModuleName)
{
	ModuleBase = (uintptr_t)::GetModuleHandle(ModuleName);

	if (ModuleBase == 0)
	{
		return;
	}

	// Install module-local ptr to our thunk
	// This means we can do
	// call [rip + offsetof(pThunkDest)]
	// and then we don't need the Va to directly follow the call
	UnprotectRegion(ModuleBase, sizeof(IMAGE_DOS_HEADER));
	*(uintptr_t *)(ModuleBase + pThunkOffset) = (uintptr_t)HookThunk;
}

HookHandler_t &
ModulePatcher::HandlerForHook(GPR_CONTEXT *Context)
{
	return HookHandlers[Context->r[Context->RIP]];
}

bool ModulePatcher::UnprotectRegion(uintptr_t Va, size_t Size)
{
	DWORD ProtectOld = 0;
	if (!::VirtualProtect((u8 *)Va, Size, PAGE_EXECUTE_READWRITE, &ProtectOld))
	{
		//Log("VirtualProtect:fail\n");
		return false;
	}

	return true;
}

void ModulePatcher::IcFlush(uintptr_t Va, size_t Size)
{
	::FlushInstructionCache(::GetModuleHandle(nullptr), (u8 *)Va, Size);
}

uintptr_t ModulePatcher::InsnLength(uintptr_t Va)
{
	hde64s hs;
	return hde64_disasm((void const *)Va, &hs);
}

uintptr_t ModulePatcher::GetContinuationInsnLength(uintptr_t Rva)
{
	uintptr_t length = 0;

	while (length < HookBytes::insn_size)
	{
		length += InsnLength(ModuleBase + length);
	}

	return length;
}

void ModulePatcher::Hook(uintptr_t Rva, HookDests_t Handlers, u32 Flags)
{
	if (ModuleBase == 0)
	{
		return;
	}

	uintptr_t Va = ModuleBase + Rva;
	uintptr_t ContinuationBuffer = 0;

	HookBytes Hook(HookBytes::OpCall);
	Hook.Call.Imm = 0 - (s32)(Rva + HookBytes::insn_size - pThunkOffset);

	if (Flags & SymbolInfo::CONTINUE_EXEC)
	{
		HookBytes Continuation(HookBytes::OpJmp);
		uintptr_t ContinuationInsnSize = GetContinuationInsnLength(Rva);
		uintptr_t ContinuationBytesRequired = ContinuationInsnSize + HookBytes::full_size;
		ContinuationBuffer = (uintptr_t)::VirtualAlloc(
			nullptr,
			ContinuationBytesRequired,
			MEM_COMMIT,
			PAGE_EXECUTE_READWRITE
			);
		Continuation.Va = Va + ContinuationInsnSize;

		// copy instructions which will be corrupted from the function being hooked
		memcpy(
			(u8 *)ContinuationBuffer,
			(u8 *)Va,
			ContinuationInsnSize
			);
		// copy the jmp from the saved insns back to the hooked func
		memcpy(
			(u8 *)ContinuationBuffer + ContinuationInsnSize,
			(u8 *)&Continuation,
			HookBytes::full_size
			);

		IcFlush(ContinuationBuffer, ContinuationBytesRequired);
	}

	u64 OriginalBytes = *(u64 *)Va;
	uintptr_t Rip = (uintptr_t)(Va + HookBytes::insn_size);

	// If we are re-patching something, maintain the first version of the original bytes
	if (HookHandlers.find(Rip) != HookHandlers.end())
	{
		OriginalBytes = std::get<2>(HookHandlers[Rip]);
	}

	HookHandlers[Rip] = std::make_tuple(Handlers, ContinuationBuffer, OriginalBytes);

	UnprotectRegion(Va, sizeof(u64));
	// Apply the hook in a single write, which should be atomic and avoid most issues.
	// Since the hook is only 6bytes, we read the 2 extra bytes first.
	OriginalBytes &= 0xffff000000000000ull;
	OriginalBytes |= *(u64 *)&Hook.Call;
	*(u64 *)Va = OriginalBytes;
	IcFlush(Va, sizeof(u64));
}

void ModulePatcher::UnHookAll()
{
	decltype(HookHandlers) HookInfo;
	std::swap(HookInfo, HookHandlers);

	ThingHandlers.clear();

	for (auto &hook : HookInfo)
	{
		uintptr_t Va = (uintptr_t)(hook.first - HookBytes::insn_size);
		uintptr_t Continuation = std::get<1>(hook.second);
		u64 OriginalBytes = std::get<2>(hook.second);

		// replace in a single write
		*(u64 *)Va = OriginalBytes;
		IcFlush(Va, sizeof(u64));

		// clean up if needed
		if (Continuation != 0)
		{
			::VirtualFree((LPVOID)Continuation, 0, MEM_RELEASE);
		}
	}
}
