#include "common.h"
#include "Hooks.h"
#include "ModulePatcher.h"

FILE *log_file = nullptr;
std::mutex g_i_mutex;

HANDLE PipeHandle = INVALID_HANDLE_VALUE;

void LogInit()
{
	fopen_s(&log_file, "c:/wlanhooks.txt", "a+");
}

void LogClose()
{
	if (log_file)
	{
		::FlushFileBuffers((HANDLE)_get_osfhandle(_fileno(log_file)));
		fclose(log_file);
	}
}

void PipeClose()
{
	::FlushFileBuffers(PipeHandle);
	::DisconnectNamedPipe(PipeHandle);
	::CloseHandle(PipeHandle);
	PipeHandle = INVALID_HANDLE_VALUE;
}

DWORD WINAPI
PipeThread(LPVOID /*lpThreadParameter*/)
{
	Log("[+] PipeThread\n");

	bool IsShuttingDown = false;

	while (!IsShuttingDown)
	{
		BOOL Status = ::ConnectNamedPipe(PipeHandle, nullptr);
		DWORD ErrorStatus = GetLastError();
		if (Status == FALSE &&
			(ErrorStatus == ERROR_PIPE_CONNECTED || ErrorStatus == ERROR_ALREADY_EXISTS))
		{
			Status = TRUE;
		}

		if (Status == FALSE)
		{
			Log("ConnectNamedPipe:%x\n", GetLastError());
			continue;
		}

		PipePacket Request, Response;
		DWORD NumBytesXfer = 0;

		Status = ::ReadFile(
			PipeHandle,
			&Request,
			sizeof(Request),
			&NumBytesXfer,
			nullptr
			);

		if (Status == FALSE || NumBytesXfer == 0)
		{
			// error which we don't send response for
			continue;
		}

		if (NumBytesXfer != sizeof(Request))
		{
			// indicate error
			Request.PacketType = Request.TransportError;
		}

		ZeroMemory(&Response, sizeof(Response));
		Response.PacketType = Response.PacketType;

		switch (Request.PacketType)
		{
		case Request.Shutdown:
			Response.CallStatus.Status = HooksRemove() ? Response.CallStatus.Success : Response.CallStatus.Fail;
			IsShuttingDown = true;
			break;

		case Request.GetStatus:
			// TODO query status
			Response.HooksStatus.Status = Response.HooksStatus.Uninitialized;
			break;

		case Request.ApplyPatch:
			Response.CallStatus.Status = HooksApply(Request.Symbol) ? Response.CallStatus.Success : Response.CallStatus.Fail;
			break;

		default:
			Response.PacketType = Response.TransportError;
			break;
		}

		Status = ::WriteFile(
			PipeHandle,
			&Response,
			sizeof(Response),
			&NumBytesXfer,
			nullptr
			);

		if (Status == FALSE || NumBytesXfer != sizeof(Response))
		{
			Log("WriteFile(%p):%x\n", PipeHandle, GetLastError());
		}

		::FlushFileBuffers(PipeHandle);

		::DisconnectNamedPipe(PipeHandle);
	}

	PipeClose();

	// Normal termination
	DllUnload();

	return 0;
}

void PipeInit()
{
	LPCTSTR PipeName = WLANHOOKER_PIPE_NAME;

	PipeHandle = ::CreateNamedPipe(
		PipeName,
		PIPE_ACCESS_DUPLEX,
		PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT | PIPE_REJECT_REMOTE_CLIENTS,
		1,
		0x1000,
		0x1000,
		0,
		nullptr
		);
	if (PipeHandle == INVALID_HANDLE_VALUE)
	{
		Log("CreateNamedPipe(%s):%p\n", PipeName, PipeHandle);
		return;
	}

	HANDLE ThreadHandle = ::CreateThread(
		nullptr,
		0,
		(LPTHREAD_START_ROUTINE)PipeThread,
		nullptr,
		0,
		nullptr
		);

	if (ThreadHandle == nullptr)
	{
		Log("CreateThread:%x\n", GetLastError());
	}
	else
	{
		::CloseHandle(ThreadHandle);
	}
}

#include "Windot11.h"

void
__stdcall
IEPConvertCipherSuite(
	GPR_CONTEXT *context
	// rcx = u32 OuiAndCipherType
	// r8  = PDOT11_CIPHER_ALGORITHM TypeOut
	)
{
	u32 OuiAndCipherType = (u32)context->r[context->RCX];
	u32 *TypeOut = (u32 *)context->r[context->R8];

	u32 Oui = OuiAndCipherType & 0xffffff;
	u32 Type = OuiAndCipherType >> 24;

	if (Oui == OUI_NIN && Type == 4 /* RSN CCMP Cipher Suite */)
	{
		*TypeOut = DOT11_CIPHER_ALGO_CCMP;
	}

	context->r[context->RAX] = 0;
	ReturnConversion(context);
}

void
__stdcall
IEPRSNConvertAuthSuite(
	GPR_CONTEXT *context
	// rcx = u32 OuiAndAuthType
	// r8  = PDOT11_AUTH_ALGORITHM TypeOut
	)
{
	u32 OuiAndCipherType = (u32)context->r[context->RCX];
	u32 *TypeOut = (u32 *)context->r[context->R8];

	u32 Oui = OuiAndCipherType & 0xffffff;
	u32 Type = OuiAndCipherType >> 24;

	// TODO can return to caller in OUI_NIN case,
	// otherwise execute original func
	if (Oui == OUI_NIN && Type == 2 /* RSN WPA2PSK Auth Suite*/)
	{
		*TypeOut = DOT11_AUTH_ALGO_RSNA_PSK;
	}

	context->r[context->RAX] = 0;
	ReturnConversion(context);
}

void
__stdcall
QueryMasterSendKey(
	GPR_CONTEXT *context
	// rdx = PVOID KeyStruct
	)
{
	// TODO configure this over pipe
	u8 const PskReplacement[] =
	{
		0x9b, 0xdb, 0x18, 0x71, 0x09, 0xdd, 0xb6, 0x53, 0xe0, 0xb9, 0xd6, 0x26, 0xe6, 0x45, 0x5b, 0x79,
		0x12, 0xa4, 0x88, 0x50, 0x35, 0x40, 0xf4, 0x4a, 0xbb, 0xdc, 0x2e, 0x39, 0x86, 0x22, 0xb1, 0x9c
	};

	uintptr_t KeyStruct = context->r[context->RDX];

	*(u32 *)KeyStruct = sizeof(PskReplacement);
	memcpy((u8 *)KeyStruct + sizeof(u32), PskReplacement, sizeof(PskReplacement));

	context->r[context->RAX] = 0;
	ReturnConversion(context);
}

u8 *PrfDataOut = nullptr;
unsigned int PrfDataOutLength = 0;

void
__stdcall
SSN_PRF_End(GPR_CONTEXT *context);

void
__stdcall
SSN_PRF_Entry(
	GPR_CONTEXT *context
	/*
	u8 *Key,
	u32 KeyLen,
	u8 *Prefix,
	u32 PrefixLen,
	u8 *DataIn,
	u32 DataInLen,
	u8 *DataOut,
	u32 DataOutLen
	*/
	)
{
	// coming from func entry
	uintptr_t *rsp = (uintptr_t *)(context + 1);

	u8 *DataOut = (u8 *)rsp[0x38 / 8];
	u32 DataOutLen = (unsigned int)rsp[0x40 / 8];

	PrfDataOut = (u8 *)DataOut;
	PrfDataOutLength = DataOutLen;
	// resume function
	ReturnConversion(context);
}

void
__stdcall
SSN_PRF_End(GPR_CONTEXT *context)
{
	// coming from hooked ret
	u8 RotTemp[3];
	memcpy(RotTemp, PrfDataOut, sizeof(RotTemp));
	memmove(PrfDataOut, PrfDataOut + 3, PrfDataOutLength - sizeof(RotTemp));
	memcpy(PrfDataOut + PrfDataOutLength - sizeof(RotTemp), RotTemp, sizeof(RotTemp));
	// return to caller
	context->r[context->RAX] = 0;
	ThingReturnConversion(context);
}

#include "ModulePatcher.h"
ModulePatcher WlanSec;

std::map<u32, HookDests_t> PatchDestMap = {
	{ WlanPatches::IEPConvertCipherSuite, { IEPConvertCipherSuite, nullptr } },
	{ WlanPatches::IEPRSNConvertAuthSuite, { IEPRSNConvertAuthSuite, nullptr } },
	{ WlanPatches::SSN_PRF, { SSN_PRF_Entry, SSN_PRF_End } },
	{ WlanPatches::QueryMasterSendKey, { QueryMasterSendKey, nullptr } }
};

bool HooksApply(SymbolInfo Symbol)
{
	Log("[+] %S\n", __FUNCTION__);
	
	if (WlanSec.ModuleBase == 0)
		WlanSec = ModulePatcher(TEXT("wlansec"));

	WlanSec.Hook(Symbol.Rva, PatchDestMap[Symbol.Id], Symbol.Flags);

	return true;
}

bool HooksRemove()
{
	Log("[+] %S\n", __FUNCTION__);

	WlanSec.UnHookAll();

	return true;
}
