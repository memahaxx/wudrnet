#include "common.h"
#include "Hooks.h"

static bool HasRun = false;

HMODULE DllHandle = nullptr;

BOOL APIENTRY DllMain(
	HMODULE hModule,
	DWORD ul_reason_for_call,
	LPVOID lpReserved
	)
{
	char const *NameTable[] = {
		"DLL_PROCESS_DETACH",
		"DLL_PROCESS_ATTACH",
		"DLL_THREAD_ATTACH ",
		"DLL_THREAD_DETACH "
	};

	Log("[+] DllMain(%p %S %p):%s\n",
		hModule, NameTable[ul_reason_for_call], lpReserved,
		HasRun ? TEXT("warm") : TEXT("cold"));

	switch (ul_reason_for_call)
	{
	case DLL_THREAD_ATTACH:
	case DLL_PROCESS_ATTACH:
		if (!HasRun)
		{
			HasRun = true;
			DllHandle = hModule;
			PipeInit();
		}
		break;

	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}

	return TRUE;
}

void DllUnload(DWORD ExitCode)
{
	Log("[+] DllUnload:%p %x\n", DllHandle, ExitCode);
	// TODO ALWAYS ENSURE ALL HOOKS ARE REMOVED HERE
	// library can unload from external causes
	FreeLibraryAndExitThread(DllHandle, ExitCode);
}
