#pragma once

#include "common.h"

#pragma pack(push, 1)

struct GPR_CONTEXT
{
	enum
	{
		RAX,
		RBX,
		RCX,
		RDX,
		RDI,
		RSI,
		RBP,
		//RSP,
		R8,
		R9,
		R10,
		R11,
		R12,
		R13,
		R14,
		R15,
		RIP,
		NUM_GPR
	};

	uintptr_t r[NUM_GPR];
};

struct HookBytes
{
	// rip-relative indirect:
	// call = ff15<imm32>, jmp = ff25<imm32>
	static u8 const OpCall[];
	static u8 const OpJmp[];
	
	struct
	{
		u8 Op[2];
		s32 Imm;
	} Call;
	uintptr_t Va;

	enum : size_t
	{
		insn_size = sizeof(decltype(Call)),
		full_size = insn_size + sizeof(decltype(Va))
	};

	HookBytes(u8 const *OpCode)
	{
		ZeroMemory(this, sizeof(*this));
		CopyMemory(Call.Op, OpCode, sizeof(Call.Op));
	}
};
static_assert(sizeof(HookBytes) == 14, "HookBytes has wrong size");

#pragma pack(pop)

typedef void(__stdcall *HookEntry_t)(GPR_CONTEXT *);
typedef std::pair<HookEntry_t, HookEntry_t> HookDests_t;
typedef std::tuple<HookDests_t, uintptr_t, u64> HookHandler_t;
typedef std::map<uintptr_t, HookHandler_t> HookMap_t;

void
__declspec(noreturn)
ReturnConversion(GPR_CONTEXT *context);

void
__declspec(noreturn)
ThingReturnConversion(GPR_CONTEXT *context);

struct ModulePatcher
{
	static HookMap_t HookHandlers;
	static HookHandler_t &HandlerForHook(GPR_CONTEXT *Context);

	uintptr_t ModuleBase;
	enum : uintptr_t { pThunkOffset = offsetof(IMAGE_DOS_HEADER, e_res2) };

	ModulePatcher();
	ModulePatcher(LPCTSTR ModuleName);

	bool UnprotectRegion(uintptr_t Va, size_t Size);
	void IcFlush(uintptr_t Va, size_t Size);
	uintptr_t InsnLength(uintptr_t Va);
	uintptr_t GetContinuationInsnLength(uintptr_t Rva);
	
	void Hook(uintptr_t Rva, HookDests_t Handlers, u32 Flags);
	void UnHookAll();
};
