#pragma once

#include "common.h"

__declspec(noreturn)
void DllUnload(DWORD ExitCode = 0);

void PipeInit();

void LogInit();
void LogClose();

extern FILE *log_file;
extern std::mutex g_i_mutex;

#define Log(fmt, ...) \
do { \
	std::lock_guard<std::mutex> lock(g_i_mutex); \
	LogInit(); \
	if (log_file) { \
		_ftprintf(log_file, TEXT( fmt ) , ##__VA_ARGS__); \
	} \
	LogClose(); \
} while (false);

bool HooksApply(SymbolInfo Symbol);
bool HooksRemove();
