#pragma once

#include <SDKDDKVer.h>
#include <tchar.h>
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>
#include <intrin.h>
#include <io.h>
#include <cstdint>
#include <cstdio>
#include <map>
#include <tuple>
#include <mutex>
#include <vector>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

#define WLANHOOKER_PIPE_NAME TEXT("\\\\.\\pipe\\wlanhooker")

#define OUI_NIN 0xe1c0a4

#pragma pack(push, 1)

namespace WlanPatches
{
	enum : u32
	{
		IEPConvertCipherSuite,
		IEPRSNConvertAuthSuite,
		SSN_PRF,
		QueryMasterSendKey
	};
}

struct SymbolInfo
{
	u32 Id;
	enum
	{
		CONTINUE_EXEC = 1,
		HOOK_RETURN = 2
	};
	u32 Flags;
	uintptr_t Rva;
};

struct PipePacket
{
	enum : u32
	{
		TransportError,
		Shutdown,
		GetStatus,
		ApplyPatch
	} PacketType;

	union
	{
		struct
		{
			enum : u32
			{
				Fail,
				Success
			} Status;
		} CallStatus;

		struct
		{
			enum EHooksStatus : u32
			{
				Uninitialized,
				TransportError,
				HookError,
				UnHooked,
				Hooked
			} Status;
		} HooksStatus;

		SymbolInfo Symbol;
	};
};

#pragma pack(pop)
