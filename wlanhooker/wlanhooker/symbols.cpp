#include "common.h"
#include "wlanhooker.h"

#ifdef UNICODE
#define DBGHELP_TRANSLATE_TCHAR
#endif

#include <DbgHelp.h>
#pragma comment(lib, "dbghelp.lib")

#define SYM_DEBUG_LOG

BOOL
CALLBACK
SymRegisterCallbackProc64(
	__in HANDLE hProcess,
	__in ULONG ActionCode,
	__in_opt ULONG64 CallbackData,
	__in_opt ULONG64 UserContext
)
{
	UNREFERENCED_PARAMETER(hProcess);
	UNREFERENCED_PARAMETER(UserContext);

	PIMAGEHLP_CBA_EVENT evt;

	// If SYMOPT_DEBUG is set, then the symbol handler will pass
	// verbose information on its attempt to load symbols.
	// This information be delivered as text strings.

	switch (ActionCode)
	{
	case CBA_EVENT:
		evt = (PIMAGEHLP_CBA_EVENT)CallbackData;
		Log("%s", (PTSTR)evt->desc);
		break;

	default:
		return FALSE;
	}

	return TRUE;
}

std::map<u32, PTSTR> WlanPatchNameMap = {
	{ WlanPatches::IEPConvertCipherSuite, TEXT("wlansec!IEPConvertCipherSuite") },
	{ WlanPatches::IEPRSNConvertAuthSuite, TEXT("wlansec!IEPRSNConvertAuthSuite") },
	{ WlanPatches::SSN_PRF, TEXT("wlansec!SSN_PRF") },
	{ WlanPatches::QueryMasterSendKey, TEXT("wlansec!QueryMasterSendKey") }
};

// This function resolves given symbols to RVAs
//
// N.B.
// symsrv.dll MUST be located in same directory as the dbghelp.dll which is being used
// (should place them both in same directory as this binary for simplicity).
//
// XXX due to some idiotic behavior of symsrv (https://groups.google.com/forum/#!topic/microsoft.public.windbg/k75DAzpVnOc)
// we must get the symbols from a process not running as a service. This means we need to feed the
// resolved symbol info over the pipe...something I was actually trying to avoid.
void SymsInit(std::vector<SymbolInfo> &Symbols)
{
	// Initialize dbghelp

	HANDLE ProcessHandle = ::GetCurrentProcess();
	PTSTR SymPath = TEXT("srv*c:\\symbols*http://msdl.microsoft.com/download/symbols");
	BOOL status;

	SymSetOptions(
		SYMOPT_UNDNAME | SYMOPT_NO_PROMPTS | SYMOPT_FAVOR_COMPRESSED
#ifdef SYM_DEBUG_LOG
		| SYMOPT_DEBUG
#endif
		);
	status = SymInitialize(ProcessHandle, SymPath, FALSE);
	status = SymRegisterCallback64(ProcessHandle, SymRegisterCallbackProc64, 0);

	// Load the symbols for the dll we're interested in

	PTSTR FileName = TEXT("wlansec.dll");
	DWORD64 ModuleBase = (DWORD64)::LoadLibrary(FileName);
	SymLoadModuleEx(ProcessHandle, nullptr, FileName, nullptr, ModuleBase, 0, nullptr, 0);

	// Ensure we have loaded symbols with enough info

	IMAGEHLP_MODULE64 ModuleInfo = { 0 };
	ModuleInfo.SizeOfStruct = sizeof(ModuleInfo);
	status = SymGetModuleInfo64(ProcessHandle, ModuleBase, &ModuleInfo);

	if (ModuleInfo.SymType != SymPdb)
	{
		Log("Failed to load pdb symbols\n");
	}

	for (auto &Sym : Symbols)
	{
		SYMBOL_INFO_PACKAGE SymbolPackage = { 0 };
		SYMBOL_INFO &Symbol = SymbolPackage.si;
		Symbol.SizeOfStruct = sizeof(Symbol);
		Symbol.MaxNameLen = MAX_SYM_NAME;

		PTSTR Name = WlanPatchNameMap[Sym.Id];

		status = SymFromName(ProcessHandle, Name, &Symbol);

		if (status == TRUE)
		{
			Sym.Rva = Symbol.Address - ModuleBase;
		}
		else
		{
			Sym.Rva = 0;
		}
#ifdef SYM_DEBUG_LOG
		Log("%i %8x %s\n", status, Sym.Rva, Name);
#endif
	}

	// Cleanup

	status = SymCleanup(ProcessHandle);

	FreeLibrary((HMODULE)ModuleBase);
}