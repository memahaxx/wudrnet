#pragma once

#include "common.h"

#define Log(fmt, ...) do { _tprintf(TEXT( fmt ) , ##__VA_ARGS__); } while (false);

bool LoadDllIntoService(PCTSTR ServiceName, PCSTR DllPath);

u32 HooksGetStatus();
bool HooksInit(std::vector<SymbolInfo> const Symbols);
bool HooksShutdown();

void SymsInit(std::vector<SymbolInfo> &Symbols);
