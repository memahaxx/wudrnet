#include "common.h"
#include "wlanhooker.h"

typedef long NTSTATUS;

typedef NTSTATUS (NTAPI *PUSER_THREAD_START_ROUTINE)(PVOID ThreadParameter);

NTSYSCALLAPI
NTSTATUS
NTAPI
NtCreateThreadEx(
	PHANDLE ThreadHandle,
	ACCESS_MASK DesiredAccess,
	PVOID /* POBJECT_ATTRIBUTES */ ObjectAttributes,
	HANDLE ProcessHandle,
	PUSER_THREAD_START_ROUTINE StartRoutine,
	PVOID Argument,
	ULONG CreateFlags,
	SIZE_T ZeroBits,
	SIZE_T StackSize,
	SIZE_T MaximumStackSize,
	PVOID /* PPS_ATTRIBUTE_LIST */ AttributeList
	);

typedef decltype(&NtCreateThreadEx) NtCreateThreadEx_t;

void ServiceHandleClose(SC_HANDLE &handle)
{
	if (handle != nullptr)
	{
		::CloseServiceHandle(handle);
		handle = nullptr;
	}
}

void HandleClose(HANDLE &handle)
{
	if (handle != nullptr)
	{
		::CloseHandle(handle);
		handle = nullptr;
	}
}

bool SetSeDebugPrivilege()
{
	HANDLE Token = nullptr;
	TOKEN_PRIVILEGES TokenPrivs;
	LUID DebugLuid;
	bool rv = false;

	if (::OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &Token))
	{
		if (::LookupPrivilegeValue(nullptr, SE_DEBUG_NAME, &DebugLuid))
		{
			TokenPrivs.PrivilegeCount = 1;
			TokenPrivs.Privileges[0].Luid = DebugLuid;
			TokenPrivs.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
			if (::AdjustTokenPrivileges(Token, FALSE, &TokenPrivs, 0, nullptr, nullptr))
			{
				rv = true;
			}
			else
			{
				Log("AdjustTokenPrivileges:%x\n", GetLastError());
			}
		}
		else
		{
			Log("LookupPrivilegeValue:%x\n", GetLastError());
		}
	}
	else
	{
		Log("OpenProcessToken:%x\n", GetLastError());
	}

	HandleClose(Token);

	return rv;
}

void OpenServiceProcess(PCTSTR ServiceName, HANDLE &Process)
{
	SC_HANDLE ServiceManager = nullptr, ServiceHandle = nullptr;
	SERVICE_STATUS_PROCESS ProcessStatus;
	DWORD BytesNeeded;

	Process = nullptr;

	ServiceManager = ::OpenSCManager(nullptr, nullptr, SC_MANAGER_ENUMERATE_SERVICE);

	if (ServiceManager == nullptr)
	{
		Log("OpenSCManager:%x\n", GetLastError());
		goto cleanup;
	}

	ServiceHandle = ::OpenService(ServiceManager, ServiceName, SERVICE_QUERY_STATUS);

	if (ServiceHandle == nullptr)
	{
		Log("OpenService:%x\n", GetLastError());
		goto cleanup;
	}

	if (!::QueryServiceStatusEx(
		ServiceHandle,
		SC_STATUS_PROCESS_INFO,
		(PBYTE)&ProcessStatus,
		sizeof(ProcessStatus),
		&BytesNeeded
		))
	{
		Log("QueryServiceStatusEx:%x\n", GetLastError());
		goto cleanup;
	}

	if (!SetSeDebugPrivilege())
	{
		goto cleanup;
	}

	Process = ::OpenProcess(
		PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_VM_READ,
		TRUE,
		ProcessStatus.dwProcessId
		);

cleanup:

	ServiceHandleClose(ServiceHandle);
	ServiceHandleClose(ServiceManager);
}

void CreateThreadInProcess(HANDLE Process, HANDLE &Thread, PCSTR DllPath)
{
	Thread = nullptr;

	size_t DllPathSize = strlen(DllPath) + 1;

	LPVOID pRemoteArgs = ::VirtualAllocEx(Process, nullptr, DllPathSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	if (pRemoteArgs == nullptr)
	{
		Log("VirtualAllocEx:%x\n", GetLastError());
	}
	SIZE_T NumBytesWritten;
	if (!::WriteProcessMemory(Process, pRemoteArgs, DllPath, DllPathSize, &NumBytesWritten) ||
		NumBytesWritten != DllPathSize)
	{
		Log("WriteProcessMemory:%x\n", GetLastError());
	}

	HMODULE Kernel32Module = ::GetModuleHandle(TEXT("kernel32.dll"));
	HMODULE NtModule = ::GetModuleHandle(TEXT("ntdll.dll"));
	if (!Kernel32Module || !NtModule)
	{
		Log("GetModuleHandle failed (kernel32/ntdll)\n");
	}
	
	auto pFn = (PUSER_THREAD_START_ROUTINE)::GetProcAddress(Kernel32Module, "LoadLibraryA");
	auto pNtCreateThreadEx = (NtCreateThreadEx_t)::GetProcAddress(NtModule, "NtCreateThreadEx");
	if (!pFn || !pNtCreateThreadEx)
	{
		Log("GetProcAddress failed (target func/NtCreateThreadEx)\n");
	}

	NTSTATUS status = pNtCreateThreadEx(
		&Thread,
		STANDARD_RIGHTS_ALL | SPECIFIC_RIGHTS_ALL,
		nullptr,
		Process,
		pFn,
		pRemoteArgs,
		0,
		0,
		0,
		0,
		nullptr
		);
	if (status != 0)
	{
		Log("NtCreateThreadEx:%x\n", status);
	}

	::WaitForSingleObject(Thread, INFINITE);

	::VirtualFreeEx(Process, pRemoteArgs, 0, MEM_RELEASE);
}

bool LoadDllIntoService(PCTSTR ServiceName, PCSTR DllPath)
{
	HANDLE Process;
	HANDLE Thread;
	bool rv = false;

	OpenServiceProcess(ServiceName, Process);
	if (Process == nullptr)
	{
		Log("OpenServiceProcess:fail\n");
		goto cleanup;
	}

	CreateThreadInProcess(Process, Thread, DllPath);
	if (Thread == nullptr)
	{
		Log("CreateThreadInProcess:fail\n");
		goto cleanup;
	}

	rv = true;

cleanup:

	HandleClose(Thread);
	HandleClose(Process);

	return rv;
}
