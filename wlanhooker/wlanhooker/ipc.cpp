#include "common.h"
#include "wlanhooker.h"

bool PipeSend(PipePacket Packet, PipePacket &Response)
{
	DWORD OutBufferSize = sizeof(Response);
	DWORD NumBytesRead = 0;

	BOOL Status = ::CallNamedPipe(
		WLANHOOKER_PIPE_NAME,
		&Packet,
		sizeof(Packet),
		&Response,
		OutBufferSize,
		&NumBytesRead,
		5 * 1000
		);

	if (Status == FALSE ||
		NumBytesRead != OutBufferSize)
	{
		return false;
	}

	return true;
}

u32 HooksGetStatus()
{
	PipePacket Packet, Response;
	Packet.PacketType = Packet.GetStatus;
	if (!PipeSend(Packet, Response))
	{
		return Response.HooksStatus.TransportError;
	}
	return Response.HooksStatus.Status;
}

bool HooksInit(std::vector<SymbolInfo> Symbols)
{
	PipePacket Packet, Response;
	Packet.PacketType = Packet.ApplyPatch;

	for (auto const &Sym : Symbols)
	{
		Packet.Symbol = Sym;

		if (!PipeSend(Packet, Response) ||
			Response.CallStatus.Status != Response.CallStatus.Success)
		{
			return false;
		}
	}
	
	return true;
}

bool HooksShutdown()
{
	PipePacket Packet, Response;
	Packet.PacketType = Packet.Shutdown;
	if (!PipeSend(Packet, Response))
	{
		return false;
	}
	return Response.CallStatus.Status == Response.CallStatus.Success;
}
