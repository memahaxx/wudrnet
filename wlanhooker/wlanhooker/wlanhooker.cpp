#include "common.h"
#include "wlanhooker.h"

// Find the full path to the file "Hooks.dll" located in the current directory,
// and attempt to inject it into the wlansvc process.
void InjectHooksIntoWlansvc()
{
	PCSTR DllName = "Hooks.dll";
	PSTR DllPath = nullptr;

	DWORD RequiredSize = GetFullPathNameA(DllName, 0, nullptr, nullptr);
	DllPath = new CHAR[RequiredSize];
	RequiredSize = GetFullPathNameA(DllName, RequiredSize, DllPath, nullptr);

	LoadDllIntoService(TEXT("wlansvc"), DllPath);

	delete[] DllPath;
}

bool EnsureDllInjected()
{
	
	if (HooksGetStatus() == 1)
	{
		InjectHooksIntoWlansvc();
	}

	return HooksGetStatus() != 1;
}

int _tmain(int argc, _TCHAR* argv[])
{
	if (!EnsureDllInjected())
	{
		Log("EnsureDllInjected failed\n");
		return 1;
	}

	Log("Hooks present\n");

	std::vector<SymbolInfo> Symbols = {
		{ WlanPatches::IEPConvertCipherSuite },
		{ WlanPatches::IEPRSNConvertAuthSuite },
		{ WlanPatches::SSN_PRF, SymbolInfo::CONTINUE_EXEC | SymbolInfo::HOOK_RETURN },
		{ WlanPatches::QueryMasterSendKey }
	};

	SymsInit(Symbols);

	Log("HooksInit:%x\n", HooksInit(Symbols));

	Log("Hooks status:%x\n", HooksGetStatus());

	if (argc > 1)
		HooksShutdown();

	return 0;
}
